# gAppsLauncher

gAppsLauncher es una aplicación para Android pensada para tener todos los accesos a las aplicaciones y webs de Google a un solo toque de distancia.

Esta aplicación usa las siguientes librerias:

  - [ViewPagerIndicator][1]
  - [ActionBarSherlock][2]
  - [CWAC ColorMixer][4]
  - [Android DataFramework][5]
  - [AndroidBillingLibrary][6]
  - [Google AdMob SDK][7]


El código fuente está licenciado bajo la licencia Apache 2.0 para que cada uno pueda coger lo que necesite.

Si vas a usar el código tal cual recuerda cambiar el valor de la constante PrivateData.PUBLIC_KEY por la tuya que encontrarás en la consola de desarrolladores de Play Store.


[1]:http://viewpagerindicator.com
[2]:http://actionbarsherlock.com
[4]:http://github.com/commonsguy/cwac-colormixer
[5]:http://github.com/javipacheco/Android-DataFramework
[6]:http://github.com/robotmedia/AndroidBillingLibrary
[7]:http://developers.google.com/mobile-ads-sdk