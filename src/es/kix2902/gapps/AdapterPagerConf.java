package es.kix2902.gapps;

import java.util.ArrayList;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import es.kix2902.gapps.drag.DragController;

public class AdapterPagerConf extends PagerAdapter {
	private final static int NUM_COLUMNAS = 3;
	private Context context;
	private DragController mDragController;
	private ArrayList<LinearLayout> mPageViews;

	public AdapterPagerConf(Context context, DragController mDragController) {
		this.context = context;
		this.mDragController = mDragController;
		mPageViews = new ArrayList<LinearLayout>();
	}

	@Override
	public int getCount() {
		return NUM_COLUMNAS;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		LayoutInflater inflater = LayoutInflater.from(context);
		LinearLayout v = (LinearLayout) inflater.inflate(R.layout.apps_grid, null);
		GridView grid = (GridView) v.findViewById(R.id.gridApps);
		grid.setAdapter(new GridAdapterConf(context, position, mDragController));

		((ViewPager) container).addView(v, 0);
		mPageViews.add(position, v);

		return v;
	}

	public View getView(int position, int id) {
		return mPageViews.get(position).findViewById(id);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object view) {
		((ViewPager) container).removeView((LinearLayout) view);
	}

	@Override
	public boolean isViewFromObject(View v, Object o) {
		return v == ((LinearLayout) o);
	}

	@Override
	public void finishUpdate(View arg0) {
	}

	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void startUpdate(View arg0) {
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return context.getResources().getString(R.string.pager_apps);

		case 1:
			return context.getResources().getString(R.string.pager_webs);

		case 2:
			return context.getResources().getString(R.string.pager_nwebs);
		}

		return null;
	}
}
