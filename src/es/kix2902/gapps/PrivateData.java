package es.kix2902.gapps;

public class PrivateData {

	private static final String PUBLIC_KEY = "YOUR-API-KEY";

	public static String getPublicKey() {
		return PUBLIC_KEY;
	}

}
