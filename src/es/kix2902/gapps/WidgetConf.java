package es.kix2902.gapps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.android.dataframework.DataFramework;
import com.android.dataframework.Entity;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.viewpagerindicator.TitlePageIndicator;

import es.kix2902.gapps.drag.DragController;
import es.kix2902.gapps.drag.DragLayer;
import es.kix2902.gapps.drag.DragSource;
import es.kix2902.gapps.drag.ImageCell;

public class WidgetConf extends SherlockActivity implements OnLongClickListener {
	private AdapterPagerConf adapter;
	private ViewPager pager;

	private DragController mDragController;
	private DragLayer mDragLayer;

	int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
	boolean widgetEdit;

	ArrayList<String> accessNativas, accessAdaptadas, accessNoAdaptadas;

	private static int MAX_BUTTONS = 6;

	private final static int LAUNCH_VALIDATE = 0;

	private AdView adView;
	private SharedPreferences prefs;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		setResult(RESULT_CANCELED);
		setContentView(R.layout.main_config);

		accessNativas = new ArrayList<String>();
		for (int ii = 0; ii < Globals.getImages(0).length; ii++) {
			String pname;
			if ((pname = Globals.checkApp(this, ii)) != null) {
				accessNativas.add(pname);
			}
		}

		accessAdaptadas = new ArrayList<String>();
		String[] tempAdaptadas = Globals.getUrls(1);
		for (int ii = 0; ii < Globals.getImages(1).length; ii++) {
			accessAdaptadas.add(tempAdaptadas[ii]);
		}

		accessNoAdaptadas = new ArrayList<String>();
		String[] tempNoAdaptadas = Globals.getUrls(2);
		for (int ii = 0; ii < Globals.getImages(2).length; ii++) {
			accessNoAdaptadas.add(tempNoAdaptadas[ii]);
		}

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
			appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
			widgetEdit = extras.getBoolean("edit", false);
		}

		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(WidgetConf.this);
		AppWidgetProviderInfo info = appWidgetManager.getAppWidgetInfo(appWidgetId);

		int prev_layout = 0;

		if (info.label.equals(getResources().getString(R.string.widget4x1))) {
			prev_layout = R.layout.prev_lay41;
			MAX_BUTTONS = Widget.MAX_SIZE;

		} else if (info.label.equals(getResources().getString(R.string.widget2x1))) {
			prev_layout = R.layout.prev_lay21;
			MAX_BUTTONS = Widget2.MAX_SIZE;

		} else if (info.label.equals(getResources().getString(R.string.widget2x2))) {
			prev_layout = R.layout.prev_lay22;
			MAX_BUTTONS = Widget3.MAX_SIZE;

		}

		LinearLayout lay_base = (LinearLayout) findViewById(R.id.lay_Prev_conf);
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(prev_layout, lay_base);

		mDragController = new DragController(this);
		mDragLayer = (DragLayer) findViewById(R.id.drag_layer);
		mDragLayer.setDragController(mDragController, MAX_BUTTONS);

		mDragController.setDragListener(mDragLayer);

		adapter = new AdapterPagerConf(WidgetConf.this, mDragController);
		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);

		TitlePageIndicator indicator = (TitlePageIndicator) findViewById(R.id.titles);
		indicator.setViewPager(pager);

		inicialize(prev_layout);

		prefs = getSharedPreferences(Globals.PREFS_NAME, Context.MODE_PRIVATE);
		adView = (AdView) this.findViewById(R.id.adView);

		if (!prefs.getBoolean("adfree", false)) {
			adView.loadAd(new AdRequest());
		}

	}

	@Override
	protected void onResume() {
		super.onResume();

		try {
			DataFramework.getInstance().open(this, getPackageName());

			if (widgetEdit) {
				List<Entity> widgetItems = DataFramework.getInstance().getEntityList("widgets", "widgetId = " + appWidgetId, "orden asc");

				for (int i = 0; i < widgetItems.size(); i++) {
					Entity item = widgetItems.get(i);

					int imgResId = getResources().getIdentifier("img_" + String.format("%02d", item.getInt("orden")) + "p", "id", getPackageName());

					int resIcon = getResources().getIdentifier(item.getString("icon"), "drawable", getPackageName());

					ImageCell img = (ImageCell) findViewById(imgResId);
					img.setImageResource(resIcon);
					img.setTag(R.id.TAG_IMAGE, resIcon);

					int list = Globals.listAccess(item.getString("launch"));
					img.setTag(R.id.TAG_LISTADO, list);

					int pos = 0;
					switch (list) {
					case 0:
						pos = accessNativas.indexOf(item.getString("launch"));
						break;

					case 1:
						pos = accessAdaptadas.indexOf(item.getString("launch"));
						break;

					case 2:
						pos = accessNoAdaptadas.indexOf(item.getString("launch"));
						break;
					}

					img.setTag(R.id.TAG_POSITION, pos);
				}

				widgetEdit = false;
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			DataFramework.getInstance().close();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.action_bar, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.mnuCancel:
			finish();
			return true;

		case R.id.mnuOk:
			Intent i = new Intent(WidgetConf.this, ValidateWidget.class);
			i.putExtra("appWidgetId", appWidgetId);
			startActivityForResult(i, LAUNCH_VALIDATE);

			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == LAUNCH_VALIDATE) {
			if (resultCode == RESULT_OK) {

				try {
					DataFramework.getInstance().open(this, getPackageName());

					List<Entity> widgetToDelConfig = DataFramework.getInstance().getEntityList("general", "widgetId = " + appWidgetId);
					if (widgetToDelConfig.size() > 0) {
						Entity widgetToDel = widgetToDelConfig.get(0);
						widgetToDel.delete();
					}

					List<Entity> widgetToDelItems = DataFramework.getInstance().getEntityList("widgets", "widgetId = " + appWidgetId);
					if (widgetToDelItems.size() > 0) {
						Iterator<Entity> iter = widgetToDelItems.iterator();
						while (iter.hasNext()) {
							Entity entToDel = (Entity) iter.next();
							entToDel.delete();
						}
					}

					Boolean settings = data.getBooleanExtra("settings", false);
					int bgSelected = data.getIntExtra("bgSelected", 0);
					int colorSelected = data.getIntExtra("colorSelected", 0);

					Entity widget = new Entity("general");
					widget.setValue("widgetId", appWidgetId);
					widget.setValue("background", bgSelected);
					widget.setValue("color", colorSelected);
					widget.setValue("settings", settings.toString());
					widget.save();

					String packageName = getPackageName();

					for (int i = 1; i <= MAX_BUTTONS; i++) {
						int resId = getResources().getIdentifier("img_" + String.format("%02d", i) + "p", "id", packageName);

						ImageCell img = (ImageCell) findViewById(resId);
						if (img != null) {
							String sPos = String.valueOf(img.getTag(R.id.TAG_POSITION));
							String sList = String.valueOf(img.getTag(R.id.TAG_LISTADO));
							String sImg = String.valueOf(img.getTag(R.id.TAG_IMAGE));

							if (!sImg.equals("null")) {
								String access = "";

								switch (Integer.parseInt(sList)) {
								case 0:
									access = accessNativas.get(Integer.parseInt(sPos));
									break;

								case 1:
									access = accessAdaptadas.get(Integer.parseInt(sPos));
									break;

								case 2:
									access = accessNoAdaptadas.get(Integer.parseInt(sPos));
									break;

								}

								Entity item = new Entity("widgets");
								item.setValue("widgetId", appWidgetId);
								item.setValue("orden", i);
								try {
									item.setValue("icon", getResources().getResourceEntryName(Integer.valueOf(sImg)));
								} catch (NotFoundException e) {
									item.setValue("icon", "adsense");
								}
								item.setValue("launch", access);
								item.save();
							}
						} else {
							break;
						}
					}

				} catch (Exception e) {
					e.printStackTrace();

				} finally {
					DataFramework.getInstance().close();
				}

				AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(WidgetConf.this);

				switch (MAX_BUTTONS) {
				case 3:
					Widget2.BuildWidget(this, appWidgetManager, new int[] { appWidgetId });
					break;

				case 6:
					Widget.BuildWidget(this, appWidgetManager, new int[] { appWidgetId });
					break;

				case 9:
					Widget3.BuildWidget(this, appWidgetManager, new int[] { appWidgetId });
					break;

				}

				Intent result = new Intent();
				result.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
				setResult(RESULT_OK, result);

				finish();
			}
		}
	}

	private void inicialize(int option) {
		switch (option) {
		case R.layout.prev_lay22:
			findViewById(R.id.img_09p).setOnLongClickListener(this);
			findViewById(R.id.img_08p).setOnLongClickListener(this);
			findViewById(R.id.img_07p).setOnLongClickListener(this);

		case R.layout.prev_lay41:
			findViewById(R.id.img_06p).setOnLongClickListener(this);
			findViewById(R.id.img_05p).setOnLongClickListener(this);
			findViewById(R.id.img_04p).setOnLongClickListener(this);

		case R.layout.prev_lay21:
			findViewById(R.id.img_03p).setOnLongClickListener(this);
			findViewById(R.id.img_02p).setOnLongClickListener(this);
			findViewById(R.id.img_01p).setOnLongClickListener(this);
		}

	}

	public boolean onLongClick(View v) {
		return startDragMove(v);
	}

	/**
	 * Start dragging a view.
	 */
	public boolean startDragMove(View v) {
		DragSource dragSource = (DragSource) v;

		// We are starting a drag. Let the DragController handle it.
		mDragController.startDrag(v, dragSource, dragSource, DragController.DRAG_ACTION_MOVE);

		return true;
	}

}
