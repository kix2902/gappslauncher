package es.kix2902.gapps.drag;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import es.kix2902.gapps.R;

/**
 * This subclass of ImageView is used to display an image on an GridView. An
 * ImageCell knows which cell on the grid it is showing and which grid it is
 * attached to Cell numbers are from 0 to NumCells-1. It also knows if it is
 * empty.
 * 
 * <p>
 * Image cells are places where images can be dragged from and dropped onto.
 * Therefore, this class implements both the DragSource and DropTarget
 * interfaces.
 * 
 */

public class ImageCell extends ImageView implements DragSource, DropTarget {

	public boolean mEmpty = true;

	/**
	 * Constructors
	 */

	public ImageCell(Context context) {
		super(context);
	}

	public ImageCell(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ImageCell(Context context, AttributeSet attrs, int style) {
		super(context, attrs, style);
	}

	@Override
	public void setImageResource(int resId) {
		if (resId != 0) {
			mEmpty = false;
		}
		super.setImageResource(resId);
	}

	@Override
	public void setImageDrawable(Drawable drawable) {
		if (drawable == null) {
			mEmpty = true;
		}
		super.setImageDrawable(drawable);
	}

	/**
 */
	// DragSource interface methods

	/**
	 * This method is called to determine if the DragSource has something to
	 * drag.
	 * 
	 * @return True if there is something to drag
	 */

	public boolean allowDrag() {
		// There is something to drag if the cell is not empty.
		return !mEmpty;
	}

	/**
	 * setDragController
	 */

	public void setDragController(DragController dragger) {
		// Do nothing. We do not need to know the controller object.
	}

	/**
	 * onDropCompleted
	 */

	public void onDropCompleted(View target, boolean success) {
		// If the drop succeeds, the image has moved elsewhere.
		// So clear the image cell.
		if (success) {

			String tag = String.valueOf(getTag());

			if (tag.equals("null") && (target instanceof DeleteZone)) {
				setImageDrawable(null);
				setTag(R.id.TAG_POSITION, null);
				setTag(R.id.TAG_IMAGE, null);
				setTag(R.id.TAG_LISTADO, null);

				mEmpty = true;
			}
		}
	}

	/**
	 * Handle an object being dropped on the DropTarget. This is the where the
	 * drawable of the dragged view gets copied into the ImageCell.
	 * 
	 * @param source
	 *            DragSource where the drag started
	 * @param x
	 *            X coordinate of the drop location
	 * @param y
	 *            Y coordinate of the drop location
	 * @param xOffset
	 *            Horizontal offset with the object being dragged where the
	 *            original touch happened
	 * @param yOffset
	 *            Vertical offset with the object being dragged where the
	 *            original touch happened
	 * @param dragView
	 *            The DragView that's being dragged around on screen.
	 * @param dragInfo
	 *            Data associated with the object being dragged
	 * 
	 */
	public void onDrop(DragSource source, int x, int y, int xOffset, int yOffset, DragView dragView, Object dragInfo) {
		setBackgroundResource(android.R.color.transparent);

		ImageCell sourceView = (ImageCell) source;

		if (sourceView != null) {
			String tag = String.valueOf(sourceView.getTag());
			mEmpty = false;

			String srcPosTemp = String.valueOf(sourceView.getTag(R.id.TAG_POSITION));
			String srcImgTemp = String.valueOf(sourceView.getTag(R.id.TAG_IMAGE));
			String srcListTemp = String.valueOf(sourceView.getTag(R.id.TAG_LISTADO));

			String myPosTemp = String.valueOf(getTag(R.id.TAG_POSITION));
			String myImgTemp = String.valueOf(getTag(R.id.TAG_IMAGE));
			String myListTemp = String.valueOf(getTag(R.id.TAG_LISTADO));

			setTag(R.id.TAG_POSITION, srcPosTemp);
			setTag(R.id.TAG_IMAGE, srcImgTemp);
			setTag(R.id.TAG_LISTADO, srcListTemp);

			setImageResource(Integer.parseInt(srcImgTemp));

			if (tag.equals("null")) {
				if (myImgTemp.equals("null")) {
					sourceView.setImageResource(0);

				} else {
					sourceView.setImageResource(Integer.parseInt(myImgTemp));
				}

				sourceView.setTag(R.id.TAG_POSITION, myPosTemp);
				sourceView.setTag(R.id.TAG_LISTADO, myListTemp);
				sourceView.setTag(R.id.TAG_IMAGE, myImgTemp);
			}
		}

	}

	/**
	 * React to a dragged object entering the area of this DropSpot. Provide the
	 * user with some visual feedback.
	 */
	public void onDragEnter(DragSource source, int x, int y, int xOffset, int yOffset, DragView dragView, Object dragInfo) {
		setBackgroundResource(R.drawable.bg_item_hover);
	}

	/**
	 * React to something being dragged over the drop target.
	 */
	public void onDragOver(DragSource source, int x, int y, int xOffset, int yOffset, DragView dragView, Object dragInfo) {
	}

	/**
	 * React to a drag
	 */
	public void onDragExit(DragSource source, int x, int y, int xOffset, int yOffset, DragView dragView, Object dragInfo) {
		setBackgroundResource(android.R.color.transparent);
	}

	/**
	 * Check if a drop action can occur at, or near, the requested location.
	 * This may be called repeatedly during a drag, so any calls should return
	 * quickly.
	 * 
	 * @param source
	 *            DragSource where the drag started
	 * @param x
	 *            X coordinate of the drop location
	 * @param y
	 *            Y coordinate of the drop location
	 * @param xOffset
	 *            Horizontal offset with the object being dragged where the
	 *            original touch happened
	 * @param yOffset
	 *            Vertical offset with the object being dragged where the
	 *            original touch happened
	 * @param dragView
	 *            The DragView that's being dragged around on screen.
	 * @param dragInfo
	 *            Data associated with the object being dragged
	 * @return True if the drop will be accepted, false otherwise.
	 */
	public boolean acceptDrop(DragSource source, int x, int y, int xOffset, int yOffset, DragView dragView, Object dragInfo) {
		String tag = String.valueOf(getTag());
		return (tag.equals("null"));
	}

	/**
	 * Estimate the surface area where this object would land if dropped at the
	 * given location.
	 * 
	 * @param source
	 *            DragSource where the drag started
	 * @param x
	 *            X coordinate of the drop location
	 * @param y
	 *            Y coordinate of the drop location
	 * @param xOffset
	 *            Horizontal offset with the object being dragged where the
	 *            original touch happened
	 * @param yOffset
	 *            Vertical offset with the object being dragged where the
	 *            original touch happened
	 * @param dragView
	 *            The DragView that's being dragged around on screen.
	 * @param dragInfo
	 *            Data associated with the object being dragged
	 * @param recycle
	 *            {@link Rect} object to be possibly recycled.
	 * @return Estimated area that would be occupied if object was dropped at
	 *         the given location. Should return null if no estimate is found,
	 *         or if this target doesn't provide estimations.
	 */
	public Rect estimateDropLocation(DragSource source, int x, int y, int xOffset, int yOffset, DragView dragView, Object dragInfo, Rect recycle) {
		return null;
	}

	/**
 */
	// Other Methods

	/**
	 * Return true if this cell is empty. If it is, it means that it will accept
	 * dropped views. It also means that there is nothing to drag.
	 * 
	 * @return boolean
	 */

	public boolean isEmpty() {
		return mEmpty;
	}

	/**
	 * Call this view's onClick listener. Return true if it was called. Clicks
	 * are ignored if the cell is empty.
	 * 
	 * @return boolean
	 */

	public boolean performClick() {
		if (!mEmpty)
			return super.performClick();
		return false;
	}

	/**
	 * Call this view's onLongClick listener. Return true if it was called.
	 * Clicks are ignored if the cell is empty.
	 * 
	 * @return boolean
	 */

	public boolean performLongClick() {
		if (!mEmpty)
			return super.performLongClick();
		return false;
	}

} // end ImageCell
