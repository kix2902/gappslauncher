package es.kix2902.gapps;

import net.robotmedia.billing.BillingController;
import net.robotmedia.billing.BillingRequest.ResponseCode;
import net.robotmedia.billing.helper.AbstractBillingActivity;
import net.robotmedia.billing.model.Transaction.PurchaseState;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.viewpagerindicator.TitlePageIndicator;

public class DashBoard extends AbstractBillingActivity {
	private AdapterPagerDash adapter;
	private ViewPager pager;

	public static final byte[] SALT = new byte[] { 26, 37, -15, -85, -54, -81, 80, -43, 7, 23, 117, -88, -45, -110, -90, -4, -11, 56, -24, 35 };

	public boolean acceptBilling = false, isPurchased = false;

	private AdView adView;
	private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);

		prefs = getSharedPreferences(Globals.PREFS_NAME, Context.MODE_PRIVATE);

		adapter = new AdapterPagerDash(DashBoard.this);
		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);

		TitlePageIndicator indicator = (TitlePageIndicator) findViewById(R.id.titles);
		indicator.setViewPager(pager);

		adView = (AdView) this.findViewById(R.id.adView);
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (!prefs.getBoolean("adfree", false)) {
			adView.loadAd(new AdRequest());
		}
	}

	public byte[] getObfuscationSalt() {
		return SALT;
	}

	public String getPublicKey() {
		return PrivateData.getPublicKey();
	}

	@Override
	public void onBillingChecked(boolean supported) {
		if (supported) {

			if (prefs == null) {
				prefs = getSharedPreferences(Globals.PREFS_NAME, Context.MODE_PRIVATE);
			}

			Editor edit = prefs.edit();
			edit.putBoolean("acceptbilling", true);
			edit.commit();

			restoreTransactions();

			Button btnAdFree = (Button) findViewById(R.id.btnAdFree);
			if (btnAdFree != null) {
				btnAdFree.setVisibility(View.VISIBLE);
			}

		} else {
			Editor edit = prefs.edit();
			edit.putBoolean("acceptbilling", false);
			edit.commit();
		}
	}

	@Override
	public void onSubscriptionChecked(boolean supported) {
	}

	@Override
	public void onPurchaseStateChanged(String itemId, PurchaseState state) {
		if (state == PurchaseState.PURCHASED) {
			if (itemId.equals("es.kix2902.gapps.adsfree")) {
				Editor edit = prefs.edit();
				edit.putBoolean("adfree", true);
				edit.commit();

				Button btnAdFree = (Button) findViewById(R.id.btnAdFree);
				if (btnAdFree != null) {
					btnAdFree.setVisibility(View.GONE);
				}

				if (adView != null) {
					adView.destroy();
				}
			}

		} else {
			if (itemId.equals("es.kix2902.gapps.adsfree")) {
				Editor edit = prefs.edit();
				edit.putBoolean("adfree", false);
				edit.commit();

				Button btnAdFree = (Button) findViewById(R.id.btnAdFree);
				if (btnAdFree != null) {
					btnAdFree.setVisibility(View.VISIBLE);
				}

				if (adView != null) {
					adView.loadAd(new AdRequest());
				}
			}

			BillingController.confirmNotifications(this, itemId);
		}
	}

	@Override
	public void onRequestPurchaseResponse(String itemId, ResponseCode response) {
		if (response == ResponseCode.RESULT_OK) {
			Toast.makeText(this, R.string.purchase_ok, Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, R.string.purchase_fail, Toast.LENGTH_SHORT).show();
		}
	}

	public void requestItem(String id) {
		requestPurchase(id);
	}
}
