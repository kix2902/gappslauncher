package es.kix2902.gapps;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

public class GridAdapterDash extends BaseAdapter implements OnClickListener, OnLongClickListener {
	Context context;

	int listado;
	ArrayList<Integer> images = new ArrayList<Integer>();
	ArrayList<String> names = new ArrayList<String>();
	ArrayList<String> access = new ArrayList<String>();

	public GridAdapterDash(Context context, int listado) {
		this.context = context;
		this.listado = listado;

		Integer[] imgTemp = Globals.getImages(listado);
		String[] nameTemp = Globals.getNames(context, listado);
		String[] accessTemp = Globals.getUrls(listado);
		int size = imgTemp.length;

		for (int i = 0; i < size; i++) {
			if (listado == 0) {
				String pname;
				if ((pname = Globals.checkApp(context, i)) != null) {
					images.add(imgTemp[i]);
					names.add(nameTemp[i]);
					access.add(pname);
				}

			} else {
				images.add(imgTemp[i]);
				names.add(nameTemp[i]);
				access.add(accessTemp[i]);
			}
		}
	}

	public int getCount() {
		return images.size();
	}

	public Integer getItem(int position) {
		return images.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.item_grid, null);
		}

		ImageView img = (ImageView) convertView.findViewById(R.id.imgGrid);
		img.setImageResource(images.get(position));
		img.setTag(position);
		img.setOnClickListener(this);
		img.setOnLongClickListener(this);

		return convertView;
	}

	public void onClick(View v) {

		SharedPreferences prefs = context.getSharedPreferences(Globals.PREFS_NAME, Context.MODE_PRIVATE);

		Boolean vibrate = prefs.getBoolean("vibrate", false);
		if (vibrate) {
			Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(50);
		}

		Intent i = Globals.generateLaunchIntent(context, access.get((Integer) v.getTag()));

		context.startActivity(i);
		((DashBoard) context).finish();
	}

	public boolean onLongClick(View v) {
		Toast.makeText(context, names.get((Integer) v.getTag()), Toast.LENGTH_SHORT).show();

		return true;
	}

}
