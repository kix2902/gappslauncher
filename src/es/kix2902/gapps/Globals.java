package es.kix2902.gapps;

import java.util.Arrays;
import java.util.List;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

public class Globals {

	public final static String PREFS_NAME = "prefsDashboard";

	// Arrays de imagenes
	private static Integer[] imgNativas = new Integer[] { R.drawable.androidify, R.drawable.authenticator, R.drawable.blogger, R.drawable.books, R.drawable.calendar, R.drawable.chrome,
			R.drawable.currents, R.drawable.drive, R.drawable.earth, R.drawable.finance, R.drawable.gesture, R.drawable.goggles, R.drawable.listen, R.drawable.mail, R.drawable.maps,
			R.drawable.movies, R.drawable.music, R.drawable.news, R.drawable.play, R.drawable.plus, R.drawable.reader, R.drawable.schemer, R.drawable.search, R.drawable.talk, R.drawable.translate,
			R.drawable.voice, R.drawable.vsearch, R.drawable.youtube, R.drawable.ytremote };
	private static Integer[] imgAdaptadas = new Integer[] { R.drawable.adsense, R.drawable.calendar, R.drawable.drive, R.drawable.finance, R.drawable.groups, R.drawable.mail, R.drawable.maps,
			R.drawable.music, R.drawable.news, R.drawable.picasa, R.drawable.plus, R.drawable.reader, R.drawable.search, R.drawable.tasks, R.drawable.translate, R.drawable.voice, R.drawable.youtube };
	private static Integer[] imgNoAdaptadas = new Integer[] { R.drawable.analytics, R.drawable.blogger, R.drawable.books, R.drawable.code, R.drawable.feedburner, R.drawable.schemer, };

	public static Integer[] getImages(int listado) {
		switch (listado) {
		case 0:
			return imgNativas;

		case 1:
			return imgAdaptadas;

		case 2:
			return imgNoAdaptadas;
		}

		return null;
	}

	// Arrays de nombres
	public static String[] getNames(Context context, int listado) {
		switch (listado) {
		case 0:
			return context.getResources().getStringArray(R.array.arrayNativas);

		case 1:
			return context.getResources().getStringArray(R.array.arrayAdaptadas);

		case 2:
			return context.getResources().getStringArray(R.array.arrayNoAdaptadas);
		}

		return null;
	}

	// pname de las apps para lanzarlas y comprobar existencia
	private static String[] pnameApps = new String[] { "com.google.android.apps.androidify", "com.google.android.apps.authenticator2", "com.google.android.apps.blogger",
			"com.google.android.apps.books", "com.google.android.calendar", "com.android.chrome", "com.google.android.apps.currents", "com.google.android.apps.docs", "com.google.earth",
			"com.google.android.apps.finance", "com.google.android.apps.gesturesearch", "com.google.android.apps.unveil", "com.google.android.apps.listen", "com.google.android.gm",
			"com.google.android.apps.maps", "com.google.android.videos", "com.google.android.music", "com.google.android.apps.genie.geniewidget", "com.android.vending",
			"com.google.android.apps.plus", "com.google.android.apps.reader", "com.google.android.apps.schemer", "com.google.android.googlequicksearchbox", "com.google.android.talk",
			"com.google.android.apps.translate", "com.google.android.apps.googlevoice", "com.google.android.voicesearch", "com.google.android.youtube", "com.google.android.ytremote" };
	private static String[] pnameApps2 = new String[] { null, "com.google.android.apps.authenticator", null, null, "com.android.calendar", null, null, null, null, null, null, null, null, null,
			"com.google.android.apps.map", null, null, null, null, null, null, null, null, null, null, null, "com.google.android.voicesearch.x", "com.google.android.youtube.googletv", null };
	private static String[] pnameApps3 = new String[] { null, null, null, null, "com.htc.calendar", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
			null, null, null, null, null, null, null, null };

	public static String checkApp(Context context, int position) {
		PackageManager pm = context.getPackageManager();
		Intent i = pm.getLaunchIntentForPackage(pnameApps[position]);

		if (i != null) {
			return pnameApps[position];

		} else if (pnameApps2[position] != null) {
			i = pm.getLaunchIntentForPackage(pnameApps2[position]);

			if (i != null) {
				return pnameApps2[position];

			} else if (pnameApps3[position] != null) {
				i = pm.getLaunchIntentForPackage(pnameApps3[position]);

				if (i != null) {
					return pnameApps3[position];
				}
			}
		}
		return null;
	}

	private static String[] urlAdaptadas = new String[] { "http://www.google.com/adsense", "http://www.google.com/calendar", "https://drive.google.com/m", "http://www.google.com/m/finance",
			"https://groups.google.com/forum/m/", "http://mail.google.com/", "https://maps.google.com", "https://play.google.com/music/listen", "http://news.google.com/news/i",
			"http://picasaweb.google.es/m/viewer", "http://plus.google.com/", "http://www.google.com/reader/i/", "http://www.google.com/m", "http://mail.google.com/tasks/android",
			"http://translate.google.com/m/translate", "https://www.google.com/voice/m", "http://m.youtube.com/" };
	private static String[] urlNoAdaptadas = new String[] { "http://www.google.com/analytics", "http://www.blogger.com", "http://books.google.com/books", "http://code.google.com/",
			"http://feedburner.google.com/", "https://www.schemer.com" };

	public static String[] getUrls(int listado) {
		switch (listado) {
		case 1:
			return urlAdaptadas;

		case 2:
			return urlNoAdaptadas;
		}

		return null;
	}

	public static int listAccess(String access) {
		int listado = 0;
		if (access.startsWith("http")) {
			int posicion = Arrays.asList(urlAdaptadas).indexOf(access);

			if (posicion >= 0) {
				listado = 1;

			} else {
				listado = 2;
			}

		} else {
			listado = 0;
		}

		return listado;
	}

	public static PendingIntent generateLaunchPendingIntent(Context context, int appWidgetId, int random, String launch) {
		Intent launchIntent = null;

		if (launch.equals("reset")) {
			launchIntent = new Intent();

		} else if (launch.equals("settings")) {
			launchIntent = new Intent(context, WidgetConf.class);
			launchIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
			launchIntent.putExtra("edit", true);
			launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		} else {
			launchIntent = generateLaunchIntent(context, launch);
		}

		PendingIntent pi = PendingIntent.getActivity(context, random, launchIntent, 0);
		return pi;
	}

	public static Intent generateLaunchIntent(Context context, String launch) {
		Intent intent = new Intent();
		if (launch.startsWith("http")) {
			intent.setAction(Intent.ACTION_VIEW);
			intent.setData(Uri.parse(launch));
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		} else {
			Intent iTemp = new Intent();
			iTemp.setAction(Intent.ACTION_MAIN);
			iTemp.addCategory(Intent.CATEGORY_LAUNCHER);

			PackageManager pm = context.getPackageManager();

			List<ResolveInfo> pmList = pm.queryIntentActivities(iTemp, 0);

			for (ResolveInfo info : pmList) {
				if (info.activityInfo.packageName.equalsIgnoreCase(launch)) {
					String className = info.activityInfo.name;

					intent.setAction(Intent.ACTION_MAIN);
					intent.addCategory(Intent.CATEGORY_LAUNCHER);
					intent.setComponent(new ComponentName(launch, className));
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

					break;
				}
			}
		}

		return intent;
	}
}
