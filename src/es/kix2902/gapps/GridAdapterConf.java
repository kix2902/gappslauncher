package es.kix2902.gapps;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;
import es.kix2902.gapps.drag.DragController;
import es.kix2902.gapps.drag.DragSource;
import es.kix2902.gapps.drag.ImageCell;

public class GridAdapterConf extends BaseAdapter implements OnLongClickListener, OnClickListener {
	Context context;
	LayoutInflater inflater;
	DragController mDragController;

	int listado;
	ArrayList<Integer> images = new ArrayList<Integer>();
	ArrayList<String> names = new ArrayList<String>();
	ArrayList<String> access = new ArrayList<String>();

	public GridAdapterConf(Context context, int listado, DragController mDragController) {
		this.context = context;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		this.mDragController = mDragController;
		this.listado = listado;

		Integer[] imgTemp = Globals.getImages(listado);
		String[] nameTemp = Globals.getNames(context, listado);
		String[] accessTemp = Globals.getUrls(listado);
		int size = imgTemp.length;

		for (int i = 0; i < size; i++) {
			if (listado == 0) {
				String pname;
				if ((pname = Globals.checkApp(context, i)) != null) {
					images.add(imgTemp[i]);
					names.add(nameTemp[i]);
					access.add(pname);
				}

			} else {
				images.add(imgTemp[i]);
				names.add(nameTemp[i]);
				access.add(accessTemp[i]);
			}
		}
	}

	public int getCount() {
		return images.size();
	}

	public Integer getItem(int position) {
		return position;
	}

	public Integer getImage(int position) {
		return images.get(position);
	}

	public String getName(int position) {
		return names.get(position);
	}

	public String getAccess(int position) {
		return access.get(position);
	}

	public int getPos(String launch) {
		return access.indexOf(launch);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = null;

		ImageCell v = (ImageCell) inflater.inflate(R.layout.item_grid_conf, null);
		v.setImageResource(images.get(position));

		v.setTag("grid");
		v.setTag(R.id.TAG_POSITION, position);
		v.setTag(R.id.TAG_IMAGE, images.get(position));
		v.setTag(R.id.TAG_LISTADO, listado);

		v.setOnClickListener(this);
		v.setOnLongClickListener(this);

		return v;
	}

	public void onClick(View v) {
		Toast.makeText(context, names.get((Integer) v.getTag(R.id.TAG_POSITION)), Toast.LENGTH_SHORT).show();
	}

	public boolean onLongClick(View v) {
		return startDragCopy(v);
	}

	/**
	 * Start dragging a view.
	 */
	public boolean startDragCopy(View v) {
		DragSource dragSource = (DragSource) v;

		// We are starting a drag. Let the DragController handle it.
		mDragController.startDrag(v, dragSource, images.get((Integer) v.getTag(R.id.TAG_POSITION)), DragController.DRAG_ACTION_COPY);

		return true;
	}
}
