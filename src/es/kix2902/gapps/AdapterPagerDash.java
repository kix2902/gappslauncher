package es.kix2902.gapps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Parcelable;
import android.os.Vibrator;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.GridView;
import android.widget.LinearLayout;

public class AdapterPagerDash extends PagerAdapter {
	private final static int NUM_COLUMNAS = 4;

	private Context context;

	public AdapterPagerDash(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		return NUM_COLUMNAS;
	}

	@SuppressLint("NewApi")
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		LayoutInflater inflater = LayoutInflater.from(context);
		LinearLayout v = (LinearLayout) inflater.inflate(R.layout.apps_grid, null);
		GridView grid = (GridView) v.findViewById(R.id.gridApps);

		switch (position) {
		case 0:
			grid.setAdapter(new GridAdapterDash(context, position));

			break;

		case 1:
			grid.setAdapter(new GridAdapterDash(context, position));

			break;

		case 2:
			grid.setAdapter(new GridAdapterDash(context, position));

			break;

		case 3:
			v = (LinearLayout) inflater.inflate(R.layout.settings, null);

			LinearLayout layVibrate = (LinearLayout) v.findViewById(R.id.layVibrate);
			final CheckBox chkVibrate = (CheckBox) v.findViewById(R.id.chkVibrate);
			Button btnAdFree = (Button) v.findViewById(R.id.btnAdFree);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
				if (!vibrator.hasVibrator()) {
					layVibrate.setVisibility(View.GONE);
				}
			}

			final SharedPreferences prefs = context.getSharedPreferences(Globals.PREFS_NAME, Context.MODE_PRIVATE);

			Boolean vibrate = prefs.getBoolean("vibrate", false);
			chkVibrate.setChecked(vibrate);

			Boolean acceptBilling = prefs.getBoolean("acceptbilling", false);
			if (acceptBilling) {
				Boolean adfree = prefs.getBoolean("adfree", false);
				if (!adfree) {
					btnAdFree.setVisibility(View.VISIBLE);
				}
			}

			layVibrate.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					chkVibrate.performClick();
				}
			});

			chkVibrate.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					Editor edit = prefs.edit();
					edit.putBoolean("vibrate", isChecked);
					edit.commit();
				}
			});

			btnAdFree.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					((DashBoard) context).requestItem("es.kix2902.gapps.adsfree");
				}
			});

			break;

		}

		((ViewPager) container).addView(v, 0);

		return v;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object view) {
		((ViewPager) container).removeView((LinearLayout) view);
	}

	@Override
	public boolean isViewFromObject(View v, Object o) {
		return v == ((LinearLayout) o);
	}

	@Override
	public void finishUpdate(View arg0) {
	}

	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void startUpdate(View arg0) {
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return context.getResources().getString(R.string.pager_apps);

		case 1:
			return context.getResources().getString(R.string.pager_webs);

		case 2:
			return context.getResources().getString(R.string.pager_nwebs);

		case 3:
			return context.getResources().getString(R.string.pager_config);
		}

		return null;
	}

}
