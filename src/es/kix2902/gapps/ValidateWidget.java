package es.kix2902.gapps;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.actionbarsherlock.app.SherlockActivity;
import com.android.dataframework.DataFramework;
import com.android.dataframework.Entity;
import com.commonsware.cwac.colormixer.ColorMixer;

public class ValidateWidget extends SherlockActivity implements OnCheckedChangeListener, OnClickListener {

	private ColorMixer mixer;
	private Button btnSave, btnBack;
	private CheckBox chkSettings;
	private RadioGroup rdbGroup;

	Integer bgSelected = null, colorSelected = null;
	Boolean settings = null;

	int appWidgetId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setResult(RESULT_CANCELED);
		setContentView(R.layout.validate_widget);
		setTitle(R.string.validate_title);

		RadioButton rdbColor = (RadioButton) findViewById(R.id.bgColor);
		rdbColor.setOnCheckedChangeListener(this);

		mixer = (ColorMixer) findViewById(R.id.mixer);

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(this);

		btnBack = (Button) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(this);

		rdbGroup = (RadioGroup) findViewById(R.id.radioBackground);
		chkSettings = (CheckBox) findViewById(R.id.chkSettings);

		try {
			appWidgetId = getIntent().getExtras().getInt("appWidgetId");
			DataFramework.getInstance().open(this, getPackageName());

			List<Entity> widgetConfig = DataFramework.getInstance().getEntityList("general", "widgetId = " + appWidgetId);
			if (widgetConfig.size() > 0) {
				Entity entity = widgetConfig.get(0);

				bgSelected = entity.getInt("background");
				switch (bgSelected) {
				case 0:
					((RadioButton) findViewById(R.id.bgDefault)).setChecked(true);

					break;

				case 1:
					((RadioButton) findViewById(R.id.bgTransparent)).setChecked(true);

					break;

				case 2:
					colorSelected = entity.getInt("color");

					((RadioButton) findViewById(R.id.bgColor)).setChecked(true);
					mixer.setColor(colorSelected);

					break;

				}

				settings = Boolean.valueOf(entity.getString("settings"));
				chkSettings.setChecked(settings);

			} else {
				((RadioButton) findViewById(R.id.bgDefault)).setChecked(true);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			DataFramework.getInstance().close();
		}

	}

	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (buttonView.isChecked()) {
			mixer.setVisibility(View.VISIBLE);

		} else {
			mixer.setVisibility(View.GONE);
		}
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnBack:
			finish();
			break;

		case R.id.btnSave:

			settings = chkSettings.isChecked();

			switch (rdbGroup.getCheckedRadioButtonId()) {
			case R.id.bgDefault:
				bgSelected = 0;

				break;

			case R.id.bgTransparent:
				bgSelected = 1;

				break;

			case R.id.bgColor:
				bgSelected = 2;
				colorSelected = mixer.getColor();

				break;
			}

			Intent i = new Intent();
			i.putExtra("settings", settings);
			i.putExtra("bgSelected", bgSelected);
			if (colorSelected != null) {
				i.putExtra("colorSelected", colorSelected);
			}

			setResult(RESULT_OK, i);
			finish();

			break;

		}
	}
}
