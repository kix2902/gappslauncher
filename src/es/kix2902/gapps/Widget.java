package es.kix2902.gapps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.text.format.Time;
import android.view.View;
import android.widget.RemoteViews;

import com.android.dataframework.DataFramework;
import com.android.dataframework.Entity;

public class Widget extends AppWidgetProvider {

	public final static int MAX_SIZE = 6;

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		BuildWidget(context, appWidgetManager, appWidgetIds);
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		for (int appWidgetId : appWidgetIds) {
			try {
				DataFramework.getInstance().open(context, context.getPackageName());

				List<Entity> widgetConfig = DataFramework.getInstance().getEntityList("general", "widgetId = " + appWidgetId);
				Entity widget = widgetConfig.get(0);
				widget.delete();

				List<Entity> widgetItems = DataFramework.getInstance().getEntityList("widgets", "widgetId = " + appWidgetId);
				Iterator<Entity> iter = widgetItems.iterator();
				while (iter.hasNext()) {
					Entity ent = (Entity) iter.next();
					ent.delete();
				}

			} catch (Exception e) {
				e.printStackTrace();

			} finally {
				DataFramework.getInstance().close();
			}
		}
	}

	public static void BuildWidget(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

		for (int appWidgetId : appWidgetIds) {
			String packageName = context.getPackageName();

			RemoteViews views = new RemoteViews(packageName, R.layout.widget_lay);

			try {
				DataFramework.getInstance().open(context, packageName);

				List<Entity> widgetConfig = DataFramework.getInstance().getEntityList("general", "widgetId = " + appWidgetId);
				if (widgetConfig.size() > 0) {

					Time t = new Time();
					t.setToNow();
					Random mRandom = new Random(appWidgetId);

					Entity widget = widgetConfig.get(0);

					int bgSelected = widget.getInt("background");
					switch (bgSelected) {
					case 0:
						views.setInt(R.id.widgetBG, "setBackgroundResource", R.drawable.appwidget_bg);

						break;

					case 1:
						views.setInt(R.id.widgetBG, "setBackgroundColor", context.getResources().getColor(android.R.color.transparent));

						break;

					case 2:
						views.setInt(R.id.widgetBG, "setBackgroundColor", widget.getInt("color"));

						break;
					}

					Boolean settings = Boolean.valueOf(widget.getString("settings"));
					if (settings) {
						views.setViewVisibility(R.id.layConf, View.VISIBLE);
						views.setOnClickPendingIntent(R.id.imgConfig, Globals.generateLaunchPendingIntent(context, appWidgetId, mRandom.nextInt(), "settings"));

					} else {
						views.setViewVisibility(R.id.layConf, View.GONE);
						views.setOnClickPendingIntent(R.id.imgConfig, Globals.generateLaunchPendingIntent(context, appWidgetId, mRandom.nextInt(), "reset"));
					}

					List<Entity> widgetItems = DataFramework.getInstance().getEntityList("widgets", "widgetId = " + appWidgetId, "orden asc");

					ArrayList<Integer> posShown = new ArrayList<Integer>(MAX_SIZE);
					if (widgetItems.size() > 0) {
						for (int i = 0; i < widgetItems.size(); i++) {
							Entity item = widgetItems.get(i);
							int layResId = context.getResources().getIdentifier("lay_" + String.format("%02d", item.getInt("orden")) + "w", "id", packageName);
							int imgResId = context.getResources().getIdentifier("img_" + String.format("%02d", item.getInt("orden")) + "w", "id", packageName);

							int resIcon = context.getResources().getIdentifier(item.getString("icon"), "drawable", packageName);

							views.setViewVisibility(layResId, View.VISIBLE);
							views.setImageViewResource(imgResId, resIcon);
							views.setOnClickPendingIntent(layResId, Globals.generateLaunchPendingIntent(context, appWidgetId, mRandom.nextInt(), item.getString("launch")));

							posShown.add(item.getInt("orden"));
						}

						for (int j = 1; j <= MAX_SIZE; j++) {
							if (!posShown.contains(j)) {
								int layResId = context.getResources().getIdentifier("lay_" + String.format("%02d", j) + "w", "id", packageName);
								int imgResId = context.getResources().getIdentifier("img_" + String.format("%02d", j) + "w", "id", packageName);

								views.setViewVisibility(layResId, View.GONE);
								views.setImageViewResource(imgResId, 0);
								views.setOnClickPendingIntent(layResId, Globals.generateLaunchPendingIntent(context, appWidgetId, mRandom.nextInt(), "reset"));
							}
						}

					} else {
						// El widget no tiene iconos asociados
						views = new RemoteViews(packageName, R.layout.widget_config);
					}

				} else {
					// El widget no existe en la base de datos
					views = new RemoteViews(packageName, R.layout.widget_config);
				}

				if (views != null) {
					appWidgetManager.updateAppWidget(appWidgetId, views);
				}

			} catch (Exception e) {
				e.printStackTrace();

			} finally {
				DataFramework.getInstance().close();
			}

		}
	}
}
